package my.service.user;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import java.util.Optional;
import my.service.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepo {

  @Autowired
  DynamoDBMapper db;

  public Optional<User> findById(String id) {
    return Optional.ofNullable(db.load(User.builder().id(id).build()));
  }

  public User save(User l) {
    db.save(l);
    return l;
  }
}
