package my.service.user;

import java.util.Optional;
import my.service.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  @Autowired
  UserRepo userRepo;

  public User save(User l) {
    return userRepo.save(l);
  }

  public Optional<User> find(String id) {
    return Optional.of(
        userRepo.findById(id)
            .orElse(User.builder().build())
    );
  }
}
