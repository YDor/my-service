package my.service.user;


import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import my.service.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@RestController
@EnableWebMvc
@RequestMapping(path = "/user")
@Slf4j
public class UserController {

  @Autowired
  UserService userService;

  @PostMapping
  public User save(@RequestBody User sl) {
    log.info("save called {}", sl);
    User saved = userService.save(sl);
    log.info("save returns {}", saved);
    return saved;
  }

  @GetMapping(path = "/{id}")
  public Optional<User> get(@PathVariable String id) {
    log.info("get called {}", id);
    Optional<User> shoppingList = userService.find(id);
    log.info("found {}", shoppingList);
    return shoppingList;
  }
}
