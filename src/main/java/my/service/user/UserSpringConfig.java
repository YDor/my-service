package my.service.user;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({UserController.class, UserService.class, UserRepo.class})
public class UserSpringConfig {

}
