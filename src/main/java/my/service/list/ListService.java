package my.service.list;

import java.util.Optional;
import my.service.list.model.ShoppingList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListService {

  @Autowired
  ListRepo listRepo;

  public ShoppingList save(ShoppingList l) {
    return listRepo.save(l);
  }

  public Optional<ShoppingList> find(String id) {
    return Optional.of(
        listRepo.findById(id)
            .orElse(ShoppingList.builder().build())
    );
  }
}
