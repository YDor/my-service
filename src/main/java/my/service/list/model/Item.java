package my.service.list.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBDocument
public class Item {

  String name;
  @Builder.Default
  int needCount = 0;
  @Builder.Default
  int doneCount = 0;
  @Builder.Default
  @DynamoDBTypeConvertedEnum
  Prio prio = Prio.MEDIUM;
}
