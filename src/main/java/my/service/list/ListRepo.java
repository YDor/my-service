package my.service.list;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import java.util.Optional;
import my.service.list.model.ShoppingList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ListRepo {

  @Autowired
  DynamoDBMapper db;

  public Optional<ShoppingList> findById(String id) {
    return Optional.ofNullable(db.load(ShoppingList.builder().id(id).build()));
  }

  public ShoppingList save(ShoppingList l) {
    db.save(l);
    return l;
  }
}
