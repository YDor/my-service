package my.service.list;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ListController.class, ListService.class, ListRepo.class})
public class ListSpringConfig {

}
