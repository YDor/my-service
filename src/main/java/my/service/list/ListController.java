package my.service.list;


import java.util.Optional;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import my.service.list.model.ShoppingList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@RestController
@EnableWebMvc
@RequestMapping(path = "/list")
@Slf4j
public class ListController {

  @Autowired
  ListService listService;

  @PostMapping
  public ShoppingList save(@RequestBody ShoppingList sl) {
    log.info("save called {}", sl);
    ShoppingList saved = listService.save(sl);
    log.info("save returns {}", saved);
    return saved;
  }

  @GetMapping(path = "/{id}")
  public Optional<ShoppingList> get(@PathVariable String id) {
    log.info("get called {}", id);
    Optional<ShoppingList> shoppingList = listService.find(id);
    log.info("found {}", shoppingList);
    return shoppingList;
  }
}
