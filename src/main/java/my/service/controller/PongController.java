package my.service.controller;


import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@RestController
@EnableWebMvc
public class PongController {
    @RequestMapping(path = "/pong", method = RequestMethod.GET)
    public Map<String, String> pong() {
        Map<String, String> pong = new HashMap<>();
        pong.put("ping", "Hello, World!");
        return pong;
    }
}
