//package my.service.list;
//
//import static de.profiforms.docxworld.monitoring.adminserver.TestUtils.contract;
//import static java.util.Arrays.asList;
//import static org.hamcrest.CoreMatchers.is;
//import static org.hamcrest.Matchers.hasSize;
//import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import de.profiforms.docxworld.monitoring.adminserver.model.docxworld.Contract;
//import de.profiforms.docxworld.monitoring.adminserver.service.ContractService;
//import de.profiforms.docxworld.monitoring.adminserver.service.EventService;
//import de.profiforms.docxworld.monitoring.adminserver.service.LoginAttemptService;
//import de.profiforms.docxworld.monitoring.adminserver.service.PasswordService;
//import de.profiforms.docxworld.monitoring.adminserver.service.SecurityService;
//import de.profiforms.docxworld.monitoring.adminserver.service.UserService;
//import java.util.List;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(ContractController.class)
//public class ListControllerTest {
//
//  @Autowired
//  private MockMvc mvc;
//
//  @MockBean
//  private ContractService contractService;
//
//  @MockBean
//  private UserService userService;
//
//  @MockBean
//  private PasswordService passwordService;
//
//  @MockBean
//  private SecurityService securityService;
//
//  @MockBean
//  private AuthenticationManager authenticationManager;
//
//  @MockBean
//  private LoginAttemptService loginAttemptService;
//
//  @MockBean
//  private EventService eventService;
//
//  @Test
//  @WithMockUser
//  public void getAllContracts_returnsMultiple() throws Exception {
//    Contract contract_1 = contract(1, 1, 1);
//    Contract contract_2 = contract(2, 2, 2);
//    Contract contract_3 = contract(3, 3, 3);
//
//    List<Contract> contracts = asList(contract_1, contract_2, contract_3);
//    given(contractService.getAllContracts()).willReturn(contracts);
//
//    mvc.perform(get("/contracts"))
//        .andExpect(status().isOk())
//        .andExpect(jsonPath("$", hasSize(3)))
//        .andExpect(jsonPath("$[0].id", is(contract_1.getId())))
//        .andExpect(jsonPath("$[1].id", is(contract_2.getId())))
//        .andExpect(jsonPath("$[2].id", is(contract_3.getId())));
//  }
//
//  @Test
//  @WithMockUser
//  public void fullContractsRTE() throws Exception {
//    given(contractService.getAllContracts()).willReturn(asList(contract(1, 1, 1)));
//
//    mvc.perform(get("/contracts"))
//        .andExpect(status().isOk())
//        .andExpect(content().string("[{\"id\":1,\"number\":1,\"customerNumber\":1}]"));
//  }
//
//  @Test
//  @WithMockUser
//  public void getAllContracts_returnsEmpty() throws Exception {
//    mvc.perform(get("/contracts"))
//        .andExpect(status().isOk())
//        .andExpect(jsonPath("$", hasSize(0)));
//  }
//}
